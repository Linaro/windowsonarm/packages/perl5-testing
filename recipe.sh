#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    git clone https://github.com/Perl/perl5
    cd perl5
    git reset --hard $version
}

build()
{
    cd win32
    nmake CCTYPE=MSVC142 test-prep
}

test()
{
    cd ../t
    file perl.exe
    ./perl.exe --version
    # disable some tests in op/stat.t, as tty is not available on gitlab
    export PERL_SKIP_TTY_TEST=1
    ./perl.exe harness | tee tests.log
}

package()
{
    mv tests.log $out_dir/
}

checkout
build
test
package
